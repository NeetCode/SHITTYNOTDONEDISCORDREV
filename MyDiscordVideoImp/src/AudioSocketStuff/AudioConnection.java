package AudioSocketStuff;

import AudioShit.SUPERAMAZINGAUDIOBUFFERTHATGETSSENT;
import com.sun.jna.ptr.PointerByReference;
import org.json.JSONObject;
import tomp2p.opuswrapper.Opus;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;
import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.nio.IntBuffer;
import java.nio.ShortBuffer;

import static Utils.Lookup.USERSPEAKINGUPDATE;

public class AudioConnection {
    public static CallWebsocketForEvents WebsocketTiedToTHisForSomeReason;
    //todo: the channel id jda has is actually a object ima see if i can get way with just the id
    String ch;
    private PointerByReference opusEncoder;
    private boolean speaking = false;
    private final byte[] silenceBytes = new byte[] {(byte)0xF8, (byte)0xFF, (byte)0xFE};
    private IAudioSendSystem sendSystem;
    public static DatagramSocket dd;

    public AudioConnection (CallWebsocketForEvents ss, String channel) {
        this.WebsocketTiedToTHisForSomeReason = ss;
        ch = channel;
        this.WebsocketTiedToTHisForSomeReason.audioConnection = this;
    }

    public void ready() {
        new Thread(()-> {
            while (!WebsocketTiedToTHisForSomeReason.ready) { // wait untill we are coinnected
                try {
                    Thread.sleep(1);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            System.out.println("PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPSetting up audio send system");
            this.udpSocket = WebsocketTiedToTHisForSomeReason.getUdpSocket();
            setupSendSystem();
            System.out.println("Setting up audio recv system");
            setupRecvSystem();
        }).start();
    }
    int timestamp = 0;
    private void setupRecvSystem() {
        try {
            dd = new DatagramSocket(58000, this.udpSocket.getInetAddress()); //todo: adress already in use? so mabye we have to redo it till we find a por tthat works

            new Thread(()->{
                while (true) {
                    if(SUPERAMAZINGAUDIOBUFFERTHATGETSSENT.CANFRAME()) {
                        try {
                            System.out.println("yes");
                            long seq = System.currentTimeMillis();
                            AudioPacket packet = new AudioPacket((char) seq, timestamp, AudioConnection.WebsocketTiedToTHisForSomeReason.sscr, SUPERAMAZINGAUDIOBUFFERTHATGETSSENT.PROVIDEFRAME());
                            DatagramPacket tosend = packet.asEncryptedUdpPacket(AudioConnection.WebsocketTiedToTHisForSomeReason.address, AudioConnection.WebsocketTiedToTHisForSomeReason.secretKey);
                            dd.send(tosend);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                    timestamp+=980;//todo: prob need to change this
                }
            }).start();
        } catch (SocketException e) {
            e.printStackTrace();
        }
    }

    private void setupSendSystem() {
        IntBuffer error = IntBuffer.allocate(4);
        opusEncoder = Opus.INSTANCE.opus_encoder_create(48000, 2, Opus.OPUS_APPLICATION_AUDIO, error);
        IAudioSendFactory factory = new DefaultSendFactory();
        sendSystem = factory.createSendSystem(new PacketProvider());
        sendSystem.start();
    }
    private class PacketProvider implements IPacketProvider
    {
        char seq = 0;           //Sequence of audio packets. Used to determine the order of the packets.
        int timestamp = 0;      //Used to sync up our packets within the same timeframe of other people talking.
        private boolean sentSilenceOnConnect = false;
        private int silenceCounter = 0;
        @Override
        public String getIdentifier()
        {
            return "audiopacketprovider";
        }

        @Override
        public DatagramSocket getUdpSocket()
        {
            return AudioConnection.this.udpSocket;
        }

        @Override
        public DatagramPacket getNextPacket(boolean changeTalking)
        {
            DatagramPacket nextPacket = null;

            try
            {
                if (sentSilenceOnConnect && SUPERAMAZINGAUDIOBUFFERTHATGETSSENT.CANPROVIDE())
                {
                    silenceCounter = -1;
                    byte[] rawAudio = SUPERAMAZINGAUDIOBUFFERTHATGETSSENT.PROVIDE20MSOFAUDIO(); //fixme: this only does 20ms at a time i need to impliment that
                    if (rawAudio == null || rawAudio.length == 0)
                    {
                        if (speaking && changeTalking)
                            setSpeaking(false);
                    }
                    else
                    {
                            rawAudio = encodeToOpus(rawAudio);
                        AudioPacket packet = new AudioPacket(seq, timestamp, AudioConnection.WebsocketTiedToTHisForSomeReason.sscr, rawAudio);
                        if (!speaking)
                            setSpeaking(true);


                        nextPacket = packet.asEncryptedUdpPacket(AudioConnection.WebsocketTiedToTHisForSomeReason.address, AudioConnection.WebsocketTiedToTHisForSomeReason.secretKey);

                        if (seq + 1 > Character.MAX_VALUE)
                            seq = 0;
                        else
                            seq++;
                    }
                }
                else if (silenceCounter > -1)
                {
                    AudioPacket packet = new AudioPacket(seq, timestamp, AudioConnection.WebsocketTiedToTHisForSomeReason.sscr, silenceBytes);

                    nextPacket = packet.asEncryptedUdpPacket(AudioConnection.WebsocketTiedToTHisForSomeReason.address, AudioConnection.WebsocketTiedToTHisForSomeReason.secretKey);

                    if (seq + 1 > Character.MAX_VALUE)
                        seq = 0;
                    else
                        seq++;

                    if (++silenceCounter > 10)
                    {
                        silenceCounter = -1;
                        sentSilenceOnConnect = true;
                    }
                }
                else if (speaking && changeTalking)
                    setSpeaking(false);
            }
            catch (Exception e)
            {
            }

            if (nextPacket != null)
                timestamp += 960; //opus frame size

            return nextPacket;
        }

        @Override
        public void onConnectionLost() {

        }

        private void setSpeaking(boolean b) {
            //todo: setup this setspeaking thingy

            speaking = b;
            //VIDPayloadstoWebsocket.SetSendingVideo(40,20,41);
            AudioConnection.WebsocketTiedToTHisForSomeReason.webSocket.sendText(new JSONObject().put("op"
            ,USERSPEAKINGUPDATE).put("d", new JSONObject().put("speaking",b).put("delay",0)).toString());
            if(!speaking) sendSiliencePackets();
        }

        private void sendSiliencePackets() {
            silenceCounter = 0;
        }


    }
    public static int video,audio,rtx = 0;
    private byte[] encodeToOpus(byte[] rawAudio) {
        ShortBuffer nonEncodedBuffer = ShortBuffer.allocate(rawAudio.length / 2);
        ByteBuffer encoded = ByteBuffer.allocate(4096);
        for (int i = 0; i < rawAudio.length; i += 2)
        {
            int firstByte =  (0x000000FF & rawAudio[i]);      //Promotes to int and handles the fact that it was unsigned.
            int secondByte = (0x000000FF & rawAudio[i + 1]);  //

            //Combines the 2 bytes into a short. Opus deals with unsigned shorts, not bytes.
            short toShort = (short) ((firstByte << 8) | secondByte);

            nonEncodedBuffer.put(toShort);
        }
        ((Buffer) nonEncodedBuffer).flip();

        //TODO: check for 0 / negative value for error.
        //960 is framesize for opius
        int result = Opus.INSTANCE.opus_encode(opusEncoder, nonEncodedBuffer, 960, encoded, encoded.capacity());

        //ENCODING STOPS HERE

        byte[] audio = new byte[result];
        encoded.get(audio);
        return audio;
    }

    DatagramSocket udpSocket;
}
