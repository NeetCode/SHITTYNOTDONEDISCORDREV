package AudioSocketStuff;

import Utils.Lookup;
import com.neovisionaries.ws.client.*;
import org.json.JSONArray;
import org.json.JSONObject;
import ss.OpCodeHandler;

import java.io.IOException;
import java.net.*;
import java.nio.ByteBuffer;
import java.util.List;
import java.util.Map;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.TimeUnit;

import static Utils.Lookup.VOICEHEARTBEET;
import static Utils.Lookup.VOICEIDENTIFY;
import static Utils.Lookup.VOICESELECTPROTOCOL;

public class CallWebsocketForEvents extends WebSocketAdapter {
    private final String endpoint;
    private String sessionId;
    private final String token;
    String channel;
    public WebSocket webSocket;
    public InetSocketAddress address;
    public int sscr;
    public byte[] secretKey;
    public boolean ready;

    public CallWebsocketForEvents(String e, String c, String t) throws SocketException {
        this.endpoint = e;
        this.token = t;
        this.channel = c;
    }

    public void StartConnection() {
        try {
            webSocket = new WebSocketFactory().createSocket("wss://"+""+endpoint.split(":80")[0]).addListener(this);
            webSocket.connect(); //note they did async connection
        } catch (IOException e) {
            e.printStackTrace();
        } catch (WebSocketException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onTextMessage(WebSocket websocket, String text) throws Exception {
                OpCodeHandler.HandleVoiceWebSocket(new JSONObject(text), this) ;
    }

    AudioConnection audioConnection;
    @Override
    public void onConnected(WebSocket websocket, Map<String, List<String>> headers) throws Exception {
        System.out.println("CONNECTED TO VOICE");
        Identify();
        audioConnection.ready();
    }

    private void Identify() {
        JSONObject connectObj = new JSONObject()
                .put("server_id", Lookup.GetOurVoiceState().getChannelID()) //this is diffrent when its not a pm call i guess this would be guild id
                .put("user_id", Lookup.GetOurVoiceState().getUserID())
                .put("session_id", Lookup.GetOurVoiceState().getSession_id())
                .put("token", token);
        System.out.println(connectObj+"is getting sent");
        webSocket.sendText(new JSONObject().put("op", VOICEIDENTIFY).put("d",connectObj).toString());
        sessionId = Lookup.GetOurVoiceState().getSession_id(); //todo: this is where sessionid gets set
    }

    @Override
    public void onDisconnected(WebSocket websocket, WebSocketFrame serverCloseFrame, WebSocketFrame clientCloseFrame, boolean closedByServer) throws Exception {
        System.out.println("DISCONECTED FROM VOICE WEBSOCKET"+serverCloseFrame);
    }
    DatagramSocket udpSocket = null;
    public DatagramSocket getUdpSocket() {
        return udpSocket;
    }
        
    public void setupKeepAlive(final long keepAliveInterval)
    {

        new Thread( () ->
        {
            while (true) {
                if (webSocket != null && webSocket.isOpen())
                    webSocket.sendText(new JSONObject().put("op", VOICEHEARTBEET).put("d", System.currentTimeMillis()).toString());
                if (udpSocket != null && !udpSocket.isClosed()) {
                    long seq = 0;
                    try {
                        ByteBuffer buffer = ByteBuffer.allocate(Long.BYTES + 1);
                        buffer.put((byte) 0xC9);
                        buffer.putLong(seq);
                        DatagramPacket keepAlivePacket = new DatagramPacket(buffer.array(), buffer.array().length, address);
                        udpSocket.send(keepAlivePacket);
                    } catch (NoRouteToHostException e) {
                        System.out.println("lost connection to audio shitty thingy, closing app");
                        System.exit(2);
                    } catch (IOException e) {
                        System.out.println(e);
                    }
                }
                try {
                    Thread.sleep(keepAliveInterval);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();

    }

    public void onReadyEvent(JSONObject d) {
        sscr = d.getInt("ssrc");
        int port = d.getInt("port");
        //Ip desc
        InetSocketAddress externalipandport;
        int tries = 0;
        do {
            tries++;
            System.out.println(tries);
            externalipandport = handleIpDiscovery(new InetSocketAddress(endpoint.split(":")[0], port));
        } while(externalipandport==null);
        System.out.println("got our extneralipanddisco done");
        final JSONObject object = new JSONObject()
                .put("protocol", "udp")
                .put("data", new JSONObject()
                        .put("address", externalipandport.getHostString())
                        .put("port", externalipandport.getPort())
                        .put("mode", "xsalsa20_poly1305"));   //Discord requires encryption
        webSocket.sendText(new JSONObject().put("op", VOICESELECTPROTOCOL).put("d",object).toString()); //sets the proptocol to udp
        //todo: i should mabye add the statuses that jda has, anohow dis part is done now
    }

    private InetSocketAddress handleIpDiscovery(InetSocketAddress inetSocketAddress)
    {
        //We will now send a packet to discord to punch a port hole in the NAT wall.
        //This is called UDP hole punching.
        try
        {
            udpSocket = new DatagramSocket();   //Use UDP, not TCP.

            //Create a byte array of length 70 containing our ssrc.
            ByteBuffer buffer = ByteBuffer.allocate(70);    //70 taken from https://github.com/Rapptz/discord.py/blob/async/discord/voice_client.py#L208
            buffer.putInt(sscr);                            //Put the ssrc that we were given into the packet to send back to discord.

            //Construct our packet to be sent loaded with the byte buffer we store the ssrc in.
            DatagramPacket discoveryPacket = new DatagramPacket(buffer.array(), buffer.array().length, inetSocketAddress);
            System.out.println("oo");
            udpSocket.send(discoveryPacket);

            //Discord responds to our packet, returning a packet containing our external ip and the port we connected through.
            DatagramPacket receivedPacket = new DatagramPacket(new byte[70], 70);   //Give a buffer the same size as the one we sent.
            udpSocket.setSoTimeout(1000);
            udpSocket.receive(receivedPacket);

            //The byte array returned by discord containing our external ip and the port that we used
            //to connect to discord with.
            byte[] received = receivedPacket.getData();

            //Example string:"   121.83.253.66                                                   ��"
            //You'll notice that there are 4 leading nulls and a large amount of nulls between the the ip and
            // the last 2 bytes. Not sure why these exist.  The last 2 bytes are the port. More info below.
            String ourIP = new String(receivedPacket.getData());//Puts the entire byte array in. nulls are converted to spaces.
            ourIP = ourIP.substring(4, ourIP.length() - 2); //Removes the SSRC of the answer package and the port that is stuck on the end of this string. (last 2 bytes are the port)
            ourIP = ourIP.trim();  //Removes the extra whitespace(nulls) attached to both sides of the IP

            //The port exists as the last 2 bytes in the packet data, and is encoded as an UNSIGNED short.
            //Furthermore, it is stored in Little Endian instead of normal Big Endian.
            //We will first need to convert the byte order from Little Endian to Big Endian (reverse the order)
            //Then we will need to deal with the fact that the bytes represent an unsigned short.
            //Java cannot deal with unsigned types, so we will have to promote the short to a higher type.
            //Options:  char or int.  I will be doing int because it is just easier to work with.
            byte[] portBytes = new byte[2];                 //The port is exactly 2 bytes in size.
            portBytes[0] = received[received.length - 1];   //Get the second byte and store as the first
            portBytes[1] = received[received.length - 2];   //Get the first byte and store as the second.
            //We have now effectively converted from Little Endian -> Big Endian by reversing the order.

            //For more information on how this is converting from an unsigned short to an int refer to:
            //http://www.darksleep.com/player/JavaAndUnsignedTypes.html
            int firstByte = (0x000000FF & ((int) portBytes[0]));    //Promotes to int and handles the fact that it was unsigned.
            int secondByte = (0x000000FF & ((int) portBytes[1]));   //

            //Combines the 2 bytes back together.
            int ourPort = (firstByte << 8) | secondByte;

            this.address = inetSocketAddress;

            return new InetSocketAddress(ourIP, ourPort);
        }
        catch (SocketException e)
        {
            System.out.println(e);
        }
        catch (IOException e)
        {
            System.out.println(e);
        }
        return null;
    }


    public void onDetails(JSONObject d) {
        JSONArray keyarray = d.getJSONArray("secret_key");
        secretKey = new byte[32];
        for (int i = 0; i < keyarray.length(); i++)
            secretKey[i] = (byte)keyarray.getInt(i);
        System.out.println("got key");
        ready = true;
        //todo: the status is now offically connecte dso the handlers in audiomanager should first be executed now.
        //todo: i mean in the ready event the setupSend and setup recive systems should be done now.

    }
}
