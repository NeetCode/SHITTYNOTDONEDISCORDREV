/*
 *     Copyright 2015-2017 Austin Keener & Michael Ritter & Florian Spieß
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package AudioSocketStuff;


import Utils.Lookup;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.NoRouteToHostException;
import java.net.SocketException;
import java.nio.channels.DatagramChannel;


/**
 * <br>This implementation uses a Java thread, named based on: {@link IPacketProvider#getIdentifier()} + " Sending Thread".
 */
public class DefaultSendSystem implements IAudioSendSystem
{
    private final IPacketProvider packetProvider;
    private Thread sendThread;

    public DefaultSendSystem(IPacketProvider packetProvider)
    {
        this.packetProvider = packetProvider;
    }



    @Override
    public void start()
    {
        final DatagramSocket udpSocket = packetProvider.getUdpSocket();

        sendThread = new Thread(Lookup.audiogroup, () ->
        {
            long lastFrameSent = System.currentTimeMillis();
            while (!udpSocket.isClosed() && !sendThread.isInterrupted())
            {
                try
                {
                    boolean changeTalking = (System.currentTimeMillis() - lastFrameSent) > 20; // for 20 ms?
                    DatagramPacket packet = packetProvider.getNextPacket(changeTalking); //todo: OHISEE basically this method gets called, in a thread all the time and sends the packet if its not null, so i need to make a buffer of rawaudio in this next packet

                    if (packet != null){
                        udpSocket.send(packet);
                        AudioConnection.dd.send(packet);
                    }
                }
                catch (NoRouteToHostException e)
                {
                    packetProvider.onConnectionLost();
                }
                catch (SocketException e)
                {
                    //Most likely the socket has been closed due to the audio connection be closed. Next iteration will kill loop.
                }
                catch (Exception e)
                {
                    System.out.println(e);
                }
                finally
                {
                    //same frametimemount =20
                    long sleepTime = (20) - (System.currentTimeMillis() - lastFrameSent);
                    if (sleepTime > 0)
                    {
                        try
                        {
                            Thread.sleep(sleepTime);
                        }
                        catch (InterruptedException e)
                        {
                            //We've been asked to stop.
                            Thread.currentThread().interrupt();
                        }
                    }
                    if (System.currentTimeMillis() < lastFrameSent + 60) // If the sending didn't took longer than 60ms (3 times the time frame)
                    {
                        lastFrameSent += 20; // increase lastFrameSent
                    }
                    else
                    {
                        lastFrameSent = System.currentTimeMillis(); // else reset lastFrameSent to current time
                    }
                }
            }
        });
        sendThread.setUncaughtExceptionHandler((thread, throwable) ->
        {
            start();
        });
        sendThread.setDaemon(true);
        sendThread.setName(packetProvider.getIdentifier() + " Sending Thread");
        sendThread.setPriority((Thread.NORM_PRIORITY + Thread.MAX_PRIORITY) / 2);
        sendThread.start();
    }

    @Override
    public void shutdown()
    {
        if (sendThread != null)
            sendThread.interrupt();
    }
}
