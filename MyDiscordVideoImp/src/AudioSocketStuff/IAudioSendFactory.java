/*
 *     Copyright 2015-2017 Austin Keener & Michael Ritter & Florian Spieß
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package AudioSocketStuff;
public interface IAudioSendFactory
{
    /**
     * instance is needed to handle the sending of UDP audio packets to discord.
     *
     *         object for proper setup and usage.
     *
     */
    IAudioSendSystem createSendSystem(IPacketProvider packetProvider);
}
