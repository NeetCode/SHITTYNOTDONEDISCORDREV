package AudioSocketStuff;

import AudioSocketStuff.AudioConnection;
import org.json.JSONObject;

public class VIDPayloadstoWebsocket {
    //todo: this is called when someone sends a video
    public static void SetSendingVideo(int VIDEO, int AUDIO, int RTX) {
        int AUDIO_SSRC = VIDEO; //i
        int VIDEO_SSRC = AUDIO; //i2
        int RTX_SSRC = RTX; //i3

        int OPCODE = 12; //this is the opcode for setting video
        JSONObject jsonObject = new JSONObject().put("op", OPCODE)
                .put("video_ssrc", VIDEO_SSRC).put("audio_ssrc",AUDIO_SSRC).put("rtx_ssrc",RTX_SSRC);

        //todo its sent to the same socket
        AudioConnection.WebsocketTiedToTHisForSomeReason.webSocket.sendText(jsonObject.toString());
    }

    //fixme: i think
    private static int convertvidossrctortxssrc(int i) {
        if (i > 0) {
            return i + 1;
        }
        return 0;
    }
}
