package Objects.Events;

import org.json.JSONObject;

import static Utils.Lookup.VoiceStates;

public class VoiceStateUpdate {
    String session_id; //diff for every user
    String UserID; //dif for every userf
    String ChannelID;

    boolean self_deaf,deaf,mute,supress,self_video,self_mute;

    public VoiceStateUpdate(JSONObject jsonObject) {
        self_deaf = jsonObject.getBoolean("self_deaf");
        UserID = jsonObject.getString("user_id");
        deaf = jsonObject.getBoolean("deaf");
        session_id = jsonObject.getString("session_id");
        mute = jsonObject.getBoolean("mute");
        supress = jsonObject.getBoolean("suppress");
        self_video = jsonObject.getBoolean("self_video");
        self_mute = jsonObject.getBoolean("self_mute");
        ChannelID = jsonObject.getString("channel_id");
        VoiceStates.put(UserID, this);
    }

    public String getChannelID() {
        return ChannelID;
    }

    public String getSession_id() {
        return session_id;
    }

    public String getUserID() {
        return UserID;
    }

    public boolean isDeaf() {
        return deaf;
    }

    public boolean isMute() {
        return mute;
    }

    public boolean isSelf_deaf() {
        return self_deaf;
    }

    public boolean isSelf_mute() {
        return self_mute;
    }

    public boolean isSelf_video() {
        return self_video;
    }

    public boolean isSupress() {
        return supress;
    }
    public boolean ISOURACCOUNT() {
        return this.UserID.startsWith("368");
    }

    @Override
    public String toString() {
        return ((ISOURACCOUNT() ? "OUR VOICESTATE UPDATED" : "USER WITH ID: "+getUserID() +" STATE UPDATED!"  ) ) + String.format("::ID:%s|%s,IM:%s|%s,IS:%s,VIDEO:%s",isDeaf(),isSelf_deaf(),isMute(),isSelf_mute(),isSupress(),isSelf_video());
    }
}
