package Utils;

import Objects.Events.VoiceStateUpdate;

import java.util.HashMap;

public class Lookup {
    public static int STARTVOICECALLOPCODE = 4;
    public static HashMap<String, VoiceStateUpdate> VoiceStates = new HashMap<>();
    public static VoiceStateUpdate GetOurVoiceState() {
        for (VoiceStateUpdate voiceStateUpdate : VoiceStates.values()) {
            if(voiceStateUpdate.ISOURACCOUNT()) return voiceStateUpdate;
        }
        return null;
    }
    public static int VOICEIDENTIFY = 0;
    public static int VOICEHEARTBEET = 3;
    public static int VOICESELECTPROTOCOL = 1;
    public static int USERSPEAKINGUPDATE = 5;
    public static ThreadGroup audiogroup = new ThreadGroup("jda-audio");
    public static int SOMEONEISDOINGVIDEO = 12;


}
