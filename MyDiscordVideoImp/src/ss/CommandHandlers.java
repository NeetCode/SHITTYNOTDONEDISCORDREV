package ss;

import AudioShit.SUPERAMAZINGAUDIOBUFFERTHATGETSSENT;
import AudioSocketStuff.AudioConnection;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import javafx.geometry.Pos;
import org.json.JSONObject;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;

import static Utils.Lookup.STARTVOICECALLOPCODE;

public class CommandHandlers {
    public static JSONObject Post(String endpoint, String data) {
        try {
            return Unirest.post(endpoint).headers(new HashMap(){{
                put("Authorization","MzY4NDkyNDI2MTM1ODYzMzA2.DN_DmA.ucU-bgrxiL0T2UCz1HVhjspxwb0");
                put("User-Agent","Memes");
                put("Content-Type", "Application/json");
            }
            }).body(data).asJson().getBody().getObject();
        } catch (UnirestException e) {
            e.printStackTrace();
        }
        return null;
    }
    public static void Handle(String content, String channel) {
        if(!content.contains(" "))content = content+" ";
        switch (content.toLowerCase().split(" ")[0]) {
            case "call": {
                Websocket.Instance.SendPayload(new JSONObject(
                        ).put("self_mute",false).put("self_deaf", true).put("guild_id", JSONObject.NULL)
                        .put("self_video", true).put("channel_id", channel), STARTVOICECALLOPCODE);
                System.out.println("starting call");

                System.out.println(Post("https://discordapp.com/api/v6/channels/"+channel+"/call/ring",new JSONObject().put("recipients", JSONObject.NULL).toString()));
                break;
            }
            case "send": {
                if(content.contains(" ")) {
                    String bytes = content.replaceFirst("send ","");
                    System.out.println();
                    SUPERAMAZINGAUDIOBUFFERTHATGETSSENT.AudioBytes.add(bytes.getBytes());
                }
                break;
            }
            case "show": {
                if (content.contains(" ")){
                    String bytes = content.replaceFirst("show ","");
                    SUPERAMAZINGAUDIOBUFFERTHATGETSSENT.VIDEOBYTE.add(bytes.getBytes());
                }
            }
            case "sa":{
                AudioConnection.audio = Integer.parseInt(content.split(" ")[1]);
                break;
            }
            case "sv": {
                AudioConnection.video = Integer.parseInt(content.split(" ")[1]);
                break;
            }
            case "sr":{
                AudioConnection.rtx = Integer.parseInt(content.split(" ")[1]);
                break;
            }
            case "loadvid": {
                new Thread(()->{

                try {
                    System.out.println("loaded vid");
                    byte[] LOT = Files.readAllBytes(Paths.get("/home/frostbyte/vid.webm"));
                    for (int x = 0; x<LOT.length; x+=100) {
                        byte[] bytes = new byte[100];
                        for(int Y = 0; Y < 100; Y++){
                            if(x+Y<LOT.length)
                            bytes[Y]=LOT[x+Y];
                        }
                        System.out.println("loaded vid2");
                        SUPERAMAZINGAUDIOBUFFERTHATGETSSENT.VIDEOBYTE.add(bytes);
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                }
                }).start();
            }
        }
    }

}
