package ss;

import AudioShit.AudioManager;
import AudioSocketStuff.AudioConnection;
import AudioSocketStuff.CallWebsocketForEvents;
import Objects.Events.VoiceStateUpdate;
import org.json.JSONObject;

import java.net.SocketException;

public class DispatchHandler {
    public static void Handle(Websocket shard, String t, JSONObject d) {
        new Thread(()->{
        }).start();
        switch (t) {
            case "MESSAGE_CREATE":{
                System.out.println("new message"+d.getString("content"));
                CommandHandlers.Handle(d.getString("content"), d.getString("channel_id"));
                break;
            }
            default: {
                System.out.println(d+t);
                break;
            }
            case "GUILD_MEMBER_ADD": {
                //{"joined_at":"2017-10-29T21:17:18.465474+00:00","roles":[],"guild_id":"367294477347586048","deaf":false,"mute":false,"user":{"id":"368492426135863306","avatar":"a_4702666bc8cb3fe093a3e6499d6c127d","username":"Holo","discriminator":"4555"}}
                break;
            }
            case "GUILD_MEMBER_REMOVE": {
                //{"guild_id":"367294477347586048","user":{"id":"368492426135863306","avatar":"a_4702666bc8cb3fe093a3e6499d6c127d","username":"Holo","discriminator":"4555"}}
                break;
            }
            case "GUILD_BAN_ADD": {
                //{"guild_id":"367294477347586048","user":{"id":"368492426135863306","avatar":"a_4702666bc8cb3fe093a3e6499d6c127d","username":"Holo","discriminator":"4555"}}
                break;
            }
            case "GUILD_BAN_REMOVE": {
                //{"guild_id":"367294477347586048","user":{"id":"368492426135863306","avatar":"a_4702666bc8cb3fe093a3e6499d6c127d","username":"Holo","discriminator":"4555"}}
                break;
            }
            case "VOICE_STATE_UPDATE": {
                //{"self_deaf":true,"user_id":"368492426135863306","deaf":false,"session_id":"6917fe32e112210084c051eb42e04e11","mute":false,"suppress":false,"self_video":false,"self_mute":true,"channel_id":"376092850565414917"}
                VoiceStateUpdate voiceStateUpdate = new VoiceStateUpdate(d);
                System.out.println(voiceStateUpdate.toString());
            break;
            }
            //this seems to be retransmitted if we dont join, prob some packet entgeirty stuff make sure packet is not lost
            case "VOICE_SERVER_UPDATE": {
                //{"endpoint":"eu-west461.discord.gg:80","channel_id":"376092850565414917","token":"7e4304a5f595f14c"}VOICE_SERVER_UPDATE
                //todo: apperently the endpoint can be null if discord is unde rheavy load i should add a check for that.
                CallWebsocketForEvents callWebsocketForEvents = null;
                try {
                    callWebsocketForEvents = new CallWebsocketForEvents(d.getString("endpoint"),d.getString("channel_id"),d.getString("token"));
                } catch (SocketException e) {
                    e.printStackTrace();
                }
                //this is never used so far
                //since its all static
                AudioConnection audioConnection = new AudioConnection(callWebsocketForEvents, d.getString("channel_id"));
               // AudioManager.SetAudioConnection(audioConnection); //setups handlers
                callWebsocketForEvents.StartConnection();
            }

        }

    }
}
