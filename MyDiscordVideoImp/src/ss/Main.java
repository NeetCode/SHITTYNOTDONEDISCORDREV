package ss;

import com.sun.jna.Platform;

import java.io.IOException;

public class Main {

    public static String OPUS_LIB_NAME;

    public static void main(String[] args) {
	// write your code here
        SetupShit();
        String token = "MzY4NDkyNDI2MTM1ODYzMzA2.DN_DmA.ucU-bgrxiL0T2UCz1HVhjspxwb0";

        Websocket websocket = new Websocket(token, 0, "wss://gateway.discord.gg");
        websocket.Connect();
    }

    private static void SetupShit() {
        init();
    }
    public static synchronized boolean init()
    {
        //todo: change this fdor production
        String nativesRoot  = null;
        try
        {
            //The libraries that this is referencing are available in the src/main/resources/opus/ folder.
            //Of course, when JDA is compiled that just becomes /opus/
            nativesRoot = "/natives/" + Platform.RESOURCE_PREFIX + "/%s";
            if (nativesRoot.contains("darwin")) //Mac
                nativesRoot += ".dylib";
            else if (nativesRoot.contains("win"))
                nativesRoot += ".dll";
            else if (nativesRoot.contains("linux"))
                nativesRoot += ".so";
            else
                throw new UnsupportedOperationException();

            NativeUtil.loadLibraryFromJar(String.format(nativesRoot, "libopus"));
        }
        catch (Throwable e)
        {
            if (e instanceof UnsupportedOperationException){
            }
            else if (e instanceof IOException)
            {
                System.out.println(e);
            }
            else if (e instanceof UnsatisfiedLinkError)
            {
                System.out.println(e);
            }
            else
            {
            }

            nativesRoot = null;
        }
        finally
        {
            System.out.println("setting opus lib name"+nativesRoot);
            OPUS_LIB_NAME = nativesRoot != null ? String.format(nativesRoot, "libopus") : null;

            return true;
        }

    }

}
