package ss;

import AudioSocketStuff.CallWebsocketForEvents;
import org.json.JSONObject;
import ss.DispatchHandler;

public class OpCodeHandler {
    public static void Handle(Websocket shard, JSONObject fromsocket) {
        switch (fromsocket.getInt("op")) {
            case 10: {
                shard.SetupHeartBeat(fromsocket.getJSONObject("d"));
                break;
            }
            case 0: {
                DispatchHandler.Handle(shard, fromsocket.getString("t"), fromsocket.getJSONObject("d"));
                break;
            }
            default: {
                System.out.println(fromsocket);
            }
        }
    }

    public static void HandleVoiceWebSocket(JSONObject jsonObject, CallWebsocketForEvents ws) {
        System.out.println("YYY"+jsonObject);
        switch (jsonObject.getInt("op")) {
            case 5: { //Someone is speaking
                //{"op":5,"d":{"speaking":true,"user_id":"132471651299229696","ssrc":7}}
                break;
            }
            case 12: { //Someone is doing video?
                //{"op":12,"d":{"audio_ssrc":7,"video_ssrc":8,"user_id":"132471651299229696"}}
                break;
            }
            case 14: { //tells us about trhe codec
                //{"op":14,"d":{"video_codec":"VP8","audio_codec":"opus"}}
                break;
            }
            case 2:  { //I think its version of ready, aswell as ip discovery seems like also set the mode to xalsa20
                //{"op":2,"d":{"modes":["xsalsa20_poly1305"],"port":50032,"ssrc":1,"ip":"88.202.186.58","heartbeat_interval":5500}}
                ws.onReadyEvent(jsonObject.getJSONObject("d"));
                break;
            }
            case 8: { //tells us about heartbeat interval
                ws.setupKeepAlive(jsonObject.getJSONObject("d").getLong("heartbeat_interval")/2);//fixMe:discord sends twice appearently
                break;
            }
            case 4: { //Connection info, accoring to jda we are done connecting the websocket by now. also this has the key
                ws.onDetails(jsonObject.getJSONObject("d"));
                break;
            }
        }
    }
}
