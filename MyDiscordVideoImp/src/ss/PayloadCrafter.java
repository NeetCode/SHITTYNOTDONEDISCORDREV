package ss;

import org.json.JSONObject;

public class PayloadCrafter {
    public static String IdentityPayload(Websocket shard) {
        JSONObject jsonObject = new JSONObject("{" +
                "    \"token\": \"my_token\"," +
                "    \"properties\": {" +
                "        \"$os\": \"linux\"," +
                "        \"$browser\": \"disco\"," +
                "        \"$device\": \"disco\"" +
                "    }," +
                "    \"compress\": false," +
                "    \"large_threshold\": 250," +
                "    \"presence\": {" +
                "        \"game\": {" +
                "            \"name\": \"Somestuffs\"," +
                "            \"type\": 0" +
                "        }," +
                "        \"status\": \"dnd\"," +
                "        \"since\": 91879201," +
                "        \"afk\": false" +
                "}}");
        jsonObject.put("token", shard.Token);
        System.out.println(jsonObject);
        return new JSONObject().put("op",2).put("d",jsonObject).toString();
    }
}
