package ss;

import com.neovisionaries.ws.client.*;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;
import java.util.Map;


public class Websocket extends WebSocketAdapter {
    public static Websocket Instance;
    int seq = 0;

    int ShardID;
    WebSocket Socket;
    public String Token;


    public enum ShardState {Creating, Created, Connecting, Connected, Disconnected, Reconnecting, Disconnecting}

    ShardState shardState;

    public Websocket(String token, int ShardID, String Gateway) {
        this.shardState = ShardState.Creating;
        this.Token = token;
        try {
            this.Socket = new WebSocketFactory().createSocket(Gateway + "?encoding=json&v=6").addListener(this);
        } catch (IOException e) {
            e.printStackTrace();
        }
        this.ShardID = ShardID;
        this.shardState = ShardState.Created;
        Instance = this;
    }

    public void Connect() {
        if (shardState == ShardState.Connected) {
            Reconnect();
        } else {
            try {
                shardState = ShardState.Connecting;
                Socket.connect();
                shardState = ShardState.Connected;
            } catch (WebSocketException e) {
                e.printStackTrace();
            }
        }
    }

    //todo: add reauthing to a shard when we reconnect.
    private void Reconnect() {
        Disconnect();
        Connect();
    }

    private void Disconnect() {
        shardState = ShardState.Disconnecting;
        Socket.disconnect();
        shardState = ShardState.Disconnected;
    }

    @Override
    public void onTextMessage(WebSocket websocket, String text) throws Exception {
        JSONObject fromsocket = new JSONObject(text);
        if (fromsocket.has("s") && !fromsocket.isNull("s")) {
            seq = fromsocket.getInt("s");
        }
        OpCodeHandler.Handle(this, fromsocket);

    }

    @Override
    public void onConnected(WebSocket websocket, Map<String, List<String>> headers) throws Exception {
        websocket.sendText(PayloadCrafter.IdentityPayload(this));
    }

    public void SetupHeartBeat(JSONObject d) {
        Thread thread = new Thread(() -> {
            while (shardState == ShardState.Connected || seq == 0) {
                this.Socket.sendText(String.format("{\"op\":1,\"d\":%s}", seq));
                System.out.println("dd");
                try {
                    Thread.sleep(d.getLong("heartbeat_interval"));
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        thread.setName(String.format("Shard %s's heartbeat", ShardID));
        thread.start();

    }

    @Override
    public void onDisconnected(WebSocket websocket, WebSocketFrame serverCloseFrame, WebSocketFrame clientCloseFrame, boolean closedByServer) throws Exception {
        System.out.println(serverCloseFrame);
    }

    public void SendPayload(JSONObject object, int startvoicecallopcode) {
        System.out.println(new JSONObject().put("op",startvoicecallopcode).put("d", object)+" GOT SENT");
        this.Socket.sendText(new JSONObject().put("op", startvoicecallopcode).put("d", object).toString());
    }

}